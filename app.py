from flask import Flask, render_template, redirect, url_for, flash, request
from flask_sqlalchemy import SQLAlchemy

app = Flask(__name__)
app.secret_key = 'supersecretkey'

# Настройка соединения с базой данных
app.config['SQLALCHEMY_DATABASE_URI'] = 'postgresql://postgres:12345@localhost:5433/prepod_rating'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False

db = SQLAlchemy(app)

# Определение моделей
class Zvanie(db.Model):
    __tablename__ = 'zvanie'
    id_zvanie = db.Column(db.Integer, primary_key=True)
    zvanie = db.Column(db.String, nullable=False)

class Kafedra(db.Model):
    __tablename__ = 'kafedra'
    id_kafedra = db.Column(db.Integer, primary_key=True)
    kafedra_name = db.Column(db.String, nullable=False)

class Dolzhnost(db.Model):
    __tablename__ = 'dolzhnost'
    id_dolzhnost = db.Column(db.Integer, primary_key=True)
    dolzhnost = db.Column(db.String, nullable=False)

class Prepod(db.Model):
    __tablename__ = 'prepod'
    id_prepod = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String, nullable=False)
    surname = db.Column(db.String, nullable=False)
    secondname = db.Column(db.String, nullable=False)
    id_dolzhnost = db.Column(db.Integer, db.ForeignKey('dolzhnost.id_dolzhnost'), nullable=False)
    id_zvanie = db.Column(db.Integer, db.ForeignKey('zvanie.id_zvanie'), nullable=False)
    id_kafedra = db.Column(db.Integer, db.ForeignKey('kafedra.id_kafedra'), nullable=False)

    dolzhnost = db.relationship('Dolzhnost', backref=db.backref('prepods', lazy=True))
    zvanie = db.relationship('Zvanie', backref=db.backref('prepods', lazy=True))
    kafedra = db.relationship('Kafedra', backref=db.backref('prepods', lazy=True))

class Pokazateli(db.Model):
    __tablename__ = 'pokazateli'
    id_pokazatel = db.Column(db.Integer, primary_key=True)
    pokazatel_name = db.Column(db.String, nullable=False)
    assessment_score = db.Column(db.REAL, nullable=False)

class BallPoPrep(db.Model):
    __tablename__ = 'ball_po_prep'
    id_prepod = db.Column(db.Integer, db.ForeignKey('prepod.id_prepod'), primary_key=True)
    id_pokazatel = db.Column(db.Integer, db.ForeignKey('pokazateli.id_pokazatel'), primary_key=True)
    ball = db.Column(db.REAL, nullable=False)

    prepod = db.relationship('Prepod', backref=db.backref('ball_po_prep', lazy=True))
    pokazatel = db.relationship('Pokazateli', backref=db.backref('ball_po_prep', lazy=True))

# Создание маршрутов
@app.route('/')
def index():
    professors = db.session.query(
        Prepod.id_prepod,
        Prepod.surname,
        Prepod.name,
        Prepod.secondname,
        Kafedra.kafedra_name,
        db.func.coalesce(db.func.avg(BallPoPrep.ball), 0).label('avg_score')
    ).outerjoin(Kafedra, Prepod.id_kafedra == Kafedra.id_kafedra
    ).outerjoin(BallPoPrep, BallPoPrep.id_prepod == Prepod.id_prepod
    ).group_by(Prepod.id_prepod, Kafedra.kafedra_name
    ).order_by(db.func.coalesce(db.func.avg(BallPoPrep.ball), 0).desc()).all()

    positions = Dolzhnost.query.all()
    titles = Zvanie.query.all()
    departments = Kafedra.query.all()
    indicators = Pokazateli.query.all()

    return render_template('index.html', professors=professors, positions=positions, titles=titles, departments=departments, indicators=indicators)

@app.route('/login', methods=['GET', 'POST'])
def login():
    if request.method == 'POST':
        username = request.form['username']
        password = request.form['password']
        if username == 'admin' and password == 'password':
            return redirect(url_for('dashboard'))
        else:
            flash('Неверное имя пользователя или пароль', 'error')
    return render_template('login.html')

@app.route('/dashboard')
def dashboard():
    professors = Prepod.query.all()
    positions = Dolzhnost.query.all()
    titles = Zvanie.query.all()
    departments = Kafedra.query.all()
    return render_template('index.html', professors=professors, positions=positions, titles=titles, departments=departments)

@app.route('/department_rating')
def department_rating():
    departments = db.session.query(
        Kafedra.kafedra_name,
        Kafedra.id_kafedra,
        db.func.coalesce(db.func.avg(BallPoPrep.ball), 0).label('avg_score')
    ).outerjoin(Prepod, Prepod.id_kafedra == Kafedra.id_kafedra
    ).outerjoin(BallPoPrep, BallPoPrep.id_prepod == Prepod.id_prepod
    ).group_by(Kafedra.id_kafedra, Kafedra.kafedra_name).order_by(db.func.coalesce(db.func.avg(BallPoPrep.ball), 0).desc()).all()

    return render_template('department_rating.html', departments=departments)

@app.route('/add_employee_form')
def add_employee_form():
    positions = Dolzhnost.query.all()
    titles = Zvanie.query.all()
    departments = Kafedra.query.all()
    return render_template('add_employee.html', positions=positions, titles=titles, departments=departments)

@app.route('/submit_employee', methods=['POST'])
def submit_employee():
    try:
        name = request.form['name']
        surname = request.form['surname']
        secondname = request.form['secondname']
        id_dolzhnost = request.form['dolzhnost']
        id_zvanie = request.form['zvanie']
        id_kafedra = request.form['kafedra']

        new_employee = Prepod(name=name, surname=surname, secondname=secondname,
                              id_dolzhnost=id_dolzhnost, id_zvanie=id_zvanie, id_kafedra=id_kafedra)
        db.session.add(new_employee)
        db.session.commit()
        flash('Сотрудник успешно добавлен', 'success')
    except Exception as e:
        flash(f'Ошибка при добавлении сотрудника: {str(e)}', 'error')
    return redirect(url_for('index'))

@app.route('/add_department_form')
def add_department_form():
    return render_template('add_department.html')

@app.route('/submit_department', methods=['POST'])
def submit_department():
    try:
        kafedra_name = request.form['kafedra_name']

        new_department = Kafedra(kafedra_name=kafedra_name)
        db.session.add(new_department)
        db.session.commit()
        flash('Кафедра успешно добавлена', 'success')
    except Exception as e:
        flash(f'Ошибка при добавлении кафедры: {str(e)}', 'error')
    return redirect(url_for('index'))

@app.route('/submit_score', methods=['POST'])
def submit_score():
    try:
        prepod_id = request.form['prepod_id']
        pokazatel_id = request.form['pokazatel']
        ball = request.form['ball']

        new_score = BallPoPrep(id_prepod=prepod_id, id_pokazatel=pokazatel_id, ball=ball)
        db.session.add(new_score)
        db.session.commit()
        flash('Показатель успешно добавлен', 'success')
    except Exception as e:
        flash(f'Ошибка при добавлении показателя: {str(e)}', 'error')
    return redirect(url_for('index'))

@app.route('/indicators', methods=['GET'])
def indicators():
    indicators = Pokazateli.query.all()
    return render_template('indicators.html', indicators=indicators)

@app.route('/professor/<int:professor_id>')
def professor_detail(professor_id):
    professor = db.session.query(
        Prepod.surname,
        Prepod.name,
        Prepod.secondname,
        Kafedra.kafedra_name,
        Dolzhnost.dolzhnost,
        Zvanie.zvanie
    ).outerjoin(Kafedra, Prepod.id_kafedra == Kafedra.id_kafedra
    ).outerjoin(Dolzhnost, Prepod.id_dolzhnost == Dolzhnost.id_dolzhnost
    ).outerjoin(Zvanie, Prepod.id_zvanie == Zvanie.id_zvanie
    ).filter(Prepod.id_prepod == professor_id).first()

    scores = db.session.query(
        Pokazateli.pokazatel_name,
        BallPoPrep.ball
    ).outerjoin(BallPoPrep, BallPoPrep.id_pokazatel == Pokazateli.id_pokazatel
    ).filter(BallPoPrep.id_prepod == professor_id).all()

    return render_template('professor_modal.html', professor=professor, scores=scores)

@app.route('/department/<int:department_id>')
def department_detail(department_id):
    department = db.session.query(
        Kafedra.kafedra_name,
        db.func.coalesce(db.func.avg(BallPoPrep.ball), 0).label('avg_score')
    ).outerjoin(Prepod, Prepod.id_kafedra == Kafedra.id_kafedra
    ).outerjoin(BallPoPrep, BallPoPrep.id_prepod == Prepod.id_prepod
    ).filter(Kafedra.id_kafedra == department_id).group_by(Kafedra.kafedra_name).first()

    professors = db.session.query(
        Prepod.id_prepod,
        Prepod.surname,
        Prepod.name,
        Prepod.secondname,
        Dolzhnost.dolzhnost,
        Zvanie.zvanie,
        db.func.coalesce(db.func.sum(BallPoPrep.ball), 0).label('total_score')
    ).outerjoin(BallPoPrep, BallPoPrep.id_prepod == Prepod.id_prepod
    ).outerjoin(Dolzhnost, Dolzhnost.id_dolzhnost == Prepod.id_dolzhnost
    ).outerjoin(Zvanie, Zvanie.id_zvanie == Prepod.id_zvanie
    ).filter(Prepod.id_kafedra == department_id).group_by(Prepod.id_prepod, Dolzhnost.dolzhnost, Zvanie.zvanie).all()

    return render_template('department_modal.html', department=department, professors=professors)

if __name__ == '__main__':
    app.run(debug=True)
