--
-- PostgreSQL database dump
--

-- Dumped from database version 16.3
-- Dumped by pg_dump version 16.3

-- Started on 2024-06-26 13:53:29

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- TOC entry 225 (class 1259 OID 16495)
-- Name: ball_po_prep; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.ball_po_prep (
    id_prepod integer NOT NULL,
    id_pokazatel integer NOT NULL,
    ball real
);


ALTER TABLE public.ball_po_prep OWNER TO postgres;

--
-- TOC entry 216 (class 1259 OID 16436)
-- Name: dolzhnost; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.dolzhnost (
    id_dolzhnost integer NOT NULL,
    dolzhnost text NOT NULL
);


ALTER TABLE public.dolzhnost OWNER TO postgres;

--
-- TOC entry 215 (class 1259 OID 16435)
-- Name: dolzhnost_id_dolzhnost_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.dolzhnost_id_dolzhnost_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE public.dolzhnost_id_dolzhnost_seq OWNER TO postgres;

--
-- TOC entry 4826 (class 0 OID 0)
-- Dependencies: 215
-- Name: dolzhnost_id_dolzhnost_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.dolzhnost_id_dolzhnost_seq OWNED BY public.dolzhnost.id_dolzhnost;


--
-- TOC entry 220 (class 1259 OID 16454)
-- Name: kafedra; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.kafedra (
    id_kafedra integer NOT NULL,
    kafedra_name text NOT NULL
);


ALTER TABLE public.kafedra OWNER TO postgres;

--
-- TOC entry 219 (class 1259 OID 16453)
-- Name: kafedra_id_kafedra_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.kafedra_id_kafedra_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE public.kafedra_id_kafedra_seq OWNER TO postgres;

--
-- TOC entry 4827 (class 0 OID 0)
-- Dependencies: 219
-- Name: kafedra_id_kafedra_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.kafedra_id_kafedra_seq OWNED BY public.kafedra.id_kafedra;


--
-- TOC entry 224 (class 1259 OID 16487)
-- Name: pokazateli; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.pokazateli (
    id_pokazatel integer NOT NULL,
    pokazatel_name text NOT NULL,
    assessment_score real
);


ALTER TABLE public.pokazateli OWNER TO postgres;

--
-- TOC entry 223 (class 1259 OID 16486)
-- Name: pokazateli_id_pokazatel_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.pokazateli_id_pokazatel_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE public.pokazateli_id_pokazatel_seq OWNER TO postgres;

--
-- TOC entry 4828 (class 0 OID 0)
-- Dependencies: 223
-- Name: pokazateli_id_pokazatel_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.pokazateli_id_pokazatel_seq OWNED BY public.pokazateli.id_pokazatel;


--
-- TOC entry 222 (class 1259 OID 16463)
-- Name: prepod; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.prepod (
    id_prepod integer NOT NULL,
    name text NOT NULL,
    surname text NOT NULL,
    secondname text,
    id_dolzhnost integer NOT NULL,
    id_zvanie integer NOT NULL,
    id_kafedra integer NOT NULL
);


ALTER TABLE public.prepod OWNER TO postgres;

--
-- TOC entry 221 (class 1259 OID 16462)
-- Name: prepod_id_prepod_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.prepod_id_prepod_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE public.prepod_id_prepod_seq OWNER TO postgres;

--
-- TOC entry 4829 (class 0 OID 0)
-- Dependencies: 221
-- Name: prepod_id_prepod_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.prepod_id_prepod_seq OWNED BY public.prepod.id_prepod;


--
-- TOC entry 218 (class 1259 OID 16445)
-- Name: zvanie; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.zvanie (
    id_zvanie integer NOT NULL,
    zvanie text NOT NULL
);


ALTER TABLE public.zvanie OWNER TO postgres;

--
-- TOC entry 217 (class 1259 OID 16444)
-- Name: zvanie_id_zvanie_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.zvanie_id_zvanie_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE public.zvanie_id_zvanie_seq OWNER TO postgres;

--
-- TOC entry 4830 (class 0 OID 0)
-- Dependencies: 217
-- Name: zvanie_id_zvanie_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.zvanie_id_zvanie_seq OWNED BY public.zvanie.id_zvanie;


--
-- TOC entry 4658 (class 2604 OID 16439)
-- Name: dolzhnost id_dolzhnost; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.dolzhnost ALTER COLUMN id_dolzhnost SET DEFAULT nextval('public.dolzhnost_id_dolzhnost_seq'::regclass);


--
-- TOC entry 4660 (class 2604 OID 16457)
-- Name: kafedra id_kafedra; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.kafedra ALTER COLUMN id_kafedra SET DEFAULT nextval('public.kafedra_id_kafedra_seq'::regclass);


--
-- TOC entry 4662 (class 2604 OID 16490)
-- Name: pokazateli id_pokazatel; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.pokazateli ALTER COLUMN id_pokazatel SET DEFAULT nextval('public.pokazateli_id_pokazatel_seq'::regclass);


--
-- TOC entry 4661 (class 2604 OID 16466)
-- Name: prepod id_prepod; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.prepod ALTER COLUMN id_prepod SET DEFAULT nextval('public.prepod_id_prepod_seq'::regclass);


--
-- TOC entry 4659 (class 2604 OID 16448)
-- Name: zvanie id_zvanie; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.zvanie ALTER COLUMN id_zvanie SET DEFAULT nextval('public.zvanie_id_zvanie_seq'::regclass);


--
-- TOC entry 4664 (class 2606 OID 16443)
-- Name: dolzhnost dolzhnost_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.dolzhnost
    ADD CONSTRAINT dolzhnost_pkey PRIMARY KEY (id_dolzhnost);


--
-- TOC entry 4668 (class 2606 OID 16461)
-- Name: kafedra kafedra_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.kafedra
    ADD CONSTRAINT kafedra_pkey PRIMARY KEY (id_kafedra);


--
-- TOC entry 4672 (class 2606 OID 16494)
-- Name: pokazateli pokazateli_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.pokazateli
    ADD CONSTRAINT pokazateli_pkey PRIMARY KEY (id_pokazatel);


--
-- TOC entry 4670 (class 2606 OID 16470)
-- Name: prepod prepod_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.prepod
    ADD CONSTRAINT prepod_pkey PRIMARY KEY (id_prepod);


--
-- TOC entry 4666 (class 2606 OID 16452)
-- Name: zvanie zvanie_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.zvanie
    ADD CONSTRAINT zvanie_pkey PRIMARY KEY (id_zvanie);


--
-- TOC entry 4676 (class 2606 OID 16503)
-- Name: ball_po_prep ball_po_prep_id_pokazatel_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.ball_po_prep
    ADD CONSTRAINT ball_po_prep_id_pokazatel_fkey FOREIGN KEY (id_pokazatel) REFERENCES public.pokazateli(id_pokazatel);


--
-- TOC entry 4677 (class 2606 OID 16498)
-- Name: ball_po_prep ball_po_prep_id_prepod_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.ball_po_prep
    ADD CONSTRAINT ball_po_prep_id_prepod_fkey FOREIGN KEY (id_prepod) REFERENCES public.prepod(id_prepod);


--
-- TOC entry 4673 (class 2606 OID 16471)
-- Name: prepod prepod_id_dolzhnost_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.prepod
    ADD CONSTRAINT prepod_id_dolzhnost_fkey FOREIGN KEY (id_dolzhnost) REFERENCES public.dolzhnost(id_dolzhnost);


--
-- TOC entry 4674 (class 2606 OID 16481)
-- Name: prepod prepod_id_kafedra_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.prepod
    ADD CONSTRAINT prepod_id_kafedra_fkey FOREIGN KEY (id_kafedra) REFERENCES public.kafedra(id_kafedra);


--
-- TOC entry 4675 (class 2606 OID 16476)
-- Name: prepod prepod_id_zvanie_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.prepod
    ADD CONSTRAINT prepod_id_zvanie_fkey FOREIGN KEY (id_zvanie) REFERENCES public.zvanie(id_zvanie);


-- Completed on 2024-06-26 13:53:30

--
-- PostgreSQL database dump complete
--

